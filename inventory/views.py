from django.shortcuts import render, redirect

# rest_framework
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
# model import
from .models import *
# serializer import
from .serializers import CategorySerializer, ParentCategorySerializer, UnitSerializer, ProductSerializer
# Create your views here.
from rest_framework import viewsets


class CategoryList(APIView):
    permission_classes = [AllowAny]
    serializers_class = [CategorySerializer, ParentCategorySerializer]
    queryset = Category.objects.all()

    def get(self, request, *args, **kwargs):
        cat_id = kwargs.get('id')
        if cat_id is not None:
            category = Category.objects.get(id=cat_id)
            serialized = CategorySerializer(category, many=False)
        else:
            category = Category.objects.all()
            serialized = CategorySerializer(category, many=True)

        return Response(serialized.data)

    def post(self, request, *args, **kwargs):

        parent_id = request.data.get('parent_id')
        new_category = request.data.get('category_name')
        new_category = Category(
            name=new_category
        )
        if parent_id is not None:
            new_category.parent = Category.objects.get(id=parent_id)
        new_category.save()
        return Response({'status': 200, 'message': 'Category Added Successfully', 'result': new_category.name})


class ParentCategory(APIView):
    permission_classes = [AllowAny]
    serializers_class = [CategorySerializer, ParentCategorySerializer]

    def get(self, request, *args, **kwargs):
        cat_id = kwargs.get('id')
        category = Category.objects.root_nodes()
        serialized = CategorySerializer(category, many=True)

        return Response(serialized.data)


class UnitsView(APIView):
    permission_classes = [AllowAny]
    serializers_class = [UnitSerializer]

    def get(self, request, *args, **kwargs):
        all_units = Units.objects.all()
        serialized = UnitSerializer(all_units, many=True)
        return Response(serialized.data)

    def post(self, request, *args, **kwargs):
        serialized = UnitSerializer(data=request.data)
        if serialized.is_valid():
            try:
                serialized.save()
            except Exception as e:
                return Response({'status': 400, 'message': str(e)})
        else:
            return Response({'status': 400, 'message': str(serialized.errors)})
        return Response({'status': 200, 'message': 'Units Added Successfully'})


class ProducsView(APIView):
    permission_classes = [AllowAny]
    serializers_class = [ProductSerializer]

    def get(self, request, *args, **kwargs):
        products = Product.objects.all()
        serialized = ProductSerializer(products, many=True)
        return Response(serialized.data)

    def post(self, request, *args, **kwargs):
        serialized = ProductSerializer(data=request.data)
        if serialized.is_valid():
            try:
                serialized.save()
            except Exception as e:
                return Response({'status': 400, 'message': str(e)})
        else:
            return Response({'status': 400, 'message': str(serialized.errors)})
        return Response({'status': 200, 'message': 'Products Added Successfully'})
