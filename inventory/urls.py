from django.urls import path, include
from . import views

urlpatterns = [

    path('category/', views.CategoryList.as_view()),
    path('category/<int:id>/', views.CategoryList.as_view()),
    path('category/parents/', views.ParentCategory.as_view()),
    path('category/parents/<int:id>/', views.CategoryList.as_view()),
    path('units/', views.UnitsView.as_view()),
    path('products/', views.ProducsView.as_view()),



]
