from django.contrib import admin
from django.contrib.admin import ModelAdmin, register

from .models import *
from mptt.admin import MPTTModelAdmin
# Register your models here.
@register(Product)
class ProductAdmin(ModelAdmin):
    list_display = ('category', 'price', 'name', 'unit')
# admin.site.register(Category, MPTTModelAdmin)
# admin.site.register(Units)
# admin.site.register(Product)
