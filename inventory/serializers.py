from .models import *
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    child = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ("id", "name", "child")
        # fields = '__all__'

    def get_child(self, obj):
        return CategorySerializer(obj.get_children(), many=True).data


class ParentCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ("id", "name",)
        # fields = '__all__'


class UnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Units
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
